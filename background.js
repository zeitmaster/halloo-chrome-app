var port,
    updates = {
        "inbox": false,
        "notes": false,
        "tracking": false
    };
if (!window.namespace_of_external_bg) {
    // Fallback, by defining fallback methods or injecting a new script:
    document.write('<script src="https://cdn.firebase.com/js/client/2.0.5/firebase.js"></script>');
}

function receivePush (newUpdates) {
    updates.inbox = true, //updates.inbox || newUpdates.inbox;
    updates.notes = updates.notes || newUpdates.notes;
    updates.tracking = updates.tracking || newUpdates.tracking;
    console.log("push received!", updates);
    chrome.browserAction.setBadgeText({text: "*"});

    if (port) {
        port.postMessage(updates);
    }
}

chrome.runtime.onInstalled.addListener(function () {
    var userId = '00000000000000000000000000000001';
    var myFirebaseRef = new Firebase("https://halloo.firebaseio.com/");
    myFirebaseRef.child("/"+userId).on("value", function(snapshot) {
//        receivePush({
//            "inbox": !Math.floor(Math.random() *2),
//            "notes": !Math.floor(Math.random() *2),
//            "tracking": !Math.floor(Math.random() *2)
//        });
        receivePush(snapshot.val());
    });
    
    /******************** Clock implementation **************************
    chrome.alarms.onAlarm.addListener(function() {
        receivePush({
            "inbox": !Math.floor(Math.random() *2),
            "notes": !Math.floor(Math.random() *2),
            "tracking": true//!Math.floor(Math.random() *2)
        });
    });
    chrome.alarms.create("my_alarm", {
        when: Date.now() + 5000,
        periodInMinutes: 1
    });
    *********************************************************************/

    chrome.browserAction.onClicked.addListener(function () {
        chrome.browserAction.setBadgeText({text: ""});
    });
});

chrome.extension.onConnect.addListener(function(theport) {
    port = theport;
    port.onMessage.addListener(function(msg) {
        //console.log("message recieved "+ msg, num);
        port.postMessage(updates);
    });
});