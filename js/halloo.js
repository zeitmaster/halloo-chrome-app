//console.log(chrome.extension.getBackgroundPage());
var Halloo = {
    _notesURL: "api/1.0/workflow/cases/505554199547624a5d03bd0087983981/notes",
    _$body: jQuery('#content'),
    //_authorization: "Basic SEFaTU5TV0RUWkxJVFNJV1dMUFdBQ1ZETURDV0RPQlQ6MzAyODU2NDQ1NTQ3NjFjZGM4NjQxYjYwMDg5OTU5MjE=",
    _token: null,
    _refreshToken: null,
    _apiURL: null,
    _pmClientID: null,
    _pmClientSecret: null,
    _pmServer: null,
    _pmWorkspace: null,
    _pmUsername: null,
    _pmPassword: null,
    _currentSection: null,
    _pendantUpdates: {},

    setPMServer: function (value) {
        if(value.substr(-1) == '/') {
            this._pmServer = value.substr(0, value.length - 1);
        } else {
            this._pmServer = value;
        }
    },
    setPMWorkspace: function (value) {
        this._pmWorkspace = value;
    },
    setPMUser: function (value) {
        this._pmUsername = value;
    },
    setPMPassword: function (value) {
        this._pmPassword = value;
    },

    _generateToken: function (refresh_token, callback) {
        var that = this;
        if (!this._pmServer || !this._pmUsername || !this._pmWorkspace || !this._pmPassword || !this._pmClientSecret
            || !this._pmClientID) {
            alert("The settings weren't fully specified.");
            return;
        }
        this.proxy.POST({
            url: this._pmServer + "/" + this._pmWorkspace + '/oauth2/token',
            params: {
                "grant_type": "password",
                "username": this._pmUsername,
                "password": this._pmPassword,
                "scope": "*"
            },
            headers: {
                "Authorization": "Basic " + btoa(that._pmClientID + ":" + that._pmClientSecret)
            }
        }, { success: function (data) {
            that._token = data["access_token"];
            that._refreshToken = data["refresh_token"];
            if (typeof callback === 'function') {
                callback(data["access_token"]);
            }
        }});
    },

    init: function () {
        var that = this;
        this.getSettings(function (data) {
            //that._authorization = "Basic" + atob(data.app_id + ":" + data.password);
            that._pmServer = data.server;
            that._pmWorkspace = data.workspace;
            that._pmPassword = data.password;
            that._pmUsername = data.username;
            that._pmClientID = data.client_id;
            that._pmClientSecret = data.app_id;
            that._apiURL = that._pmServer + '/api/1.0/' + that._pmWorkspace;
            that._generateToken();
        });
        chrome.browserAction.onClicked.addListener(function () {
            console.log("onClicked: ", arguments);
        });
    },

    proxy: {
        xhr: new XMLHttpRequest(),
        request: function (method, settings, callback) {
            var finalSettings = {
                url: settings.url,
                params: settings.params || {},
                headers: settings.headers || {},
                formData: settings.formData || {}
            }, key, params = "", formData = new FormData();
            callback = callback || {};
            this.xhr.abort();

            this.xhr.open(method, finalSettings.url, true);

            for (key in finalSettings.headers) {
                if (finalSettings.headers.hasOwnProperty(key)) {
                    this.xhr.setRequestHeader(key, finalSettings.headers[key]);
                }
            }

            for (key in finalSettings.params) {
                if (finalSettings.params.hasOwnProperty(key)) {
                    if (method === 'GET') {
                        params += key + "=" + encodeURIComponent(finalSettings.params[key]) + "&";
                    } else {
                        formData.append(key, finalSettings.params[key]);
                    }
                }
            }

            this.xhr.onload = function (e) {
                try {
                    var xhr = e.target, response = JSON.parse(xhr.responseText);

                    if (xhr.status === 200) {
                        if (typeof callback.success === 'function') {
                            callback.success(response);
                        }
                    } else {
                        if (typeof callback.failure === 'function') {
                            callback.failure(response.error.message, response.error.code);
                        }
                    }
                } catch (e) {
                    callback.error();
                }

                //console.log("xxx");
            };
            this.xhr.onerror = function (e) {
                var xhr = e.target;
                console.log("xxxxxxxxx");
                if (typeof callback.error === 'function') {
                    callback.error();
                }
            };
            if (typeof callback.before === 'function') {
                callback.before();
            }
            this.xhr.send(method === 'GET' ? params || null : formData);
        },
        GET: function (settings, callback) {
            this.request("GET", settings, callback);
        },
        PUT: function (settings, callback) {
            this.request("PUT", settings, callback);
        },
        POST: function (settings, callback) {
            this.request("POST", settings, callback);
        }
    },
    consume: function (settings, callbacks) {
        this.proxy[settings.method]({
            url: this._apiURL + "/" + settings.endpoint,
            params: settings.params,
            headers: {
                "Authorization": "Bearer " + this._token
            }
        }, callbacks);
    },
    clearBody: function () {
        this._$body.empty();
    },

    addItemToBody: function (item) {
        var html = item.getHTML();
        $(html).slideUp(0);
        this._$body.append(item.getHTML());
        $(html).slideDown();
    },

    /*processNotes: function (e) {
        var data = JSON.parse(e.target.responseText), note, item, i;
        this.clearBody();
        for (i = 0; i < data.length; i += 1) {
            item = data[i],
            note = new Note({
                author: item.usr_uid,
                message: item.note_content,
                datetime: item.note_date
            });
            this.addItemToBody(note);
        }
    },*/

    getToken: function (callback) {
        if (this._token) {
            callback(this._token);
        } else {
            this._generateToken(null, callback);
        }
    },

    /*loadNotes: function () {
        var that = this;
        this.getToken(function (token) {
            that.proxy.GET({
                url: that._pmServer + '/' + that._notesURL,
                headers: {
                    'Authorization': "Bearer " + token
                }
            }, {
                success: that.processNotes.bind(that)
            });
        });
    },*/
    saveSettings: function (settings, callback) {
        chrome.storage.sync.set({
            server: settings.server,
            client_id: settings.client_id,
            app_id: settings.app_id,
            workspace: settings.workspace,
            username: settings.username,
            password: settings.password,
        }, callback);
    },
    getSettings: function (callback) {
        chrome.storage.sync.get(["server", "workspace", "client_id", "app_id", "username", "password"], callback);
    },
    addToCache: function (cacheName, data, callback) {
        var storageName = cacheName + "_cache", defaultDictionary = {};
        defaultDictionary[storageName] = [];
        chrome.storage.sync.get(defaultDictionary, function (d) {
            var arrayData = d[storageName];
            arrayData = data.concat(arrayData);
            defaultDictionary[storageName] = arrayData;
            chrome.storage.sync.set(defaultDictionary, callback);
        });
    },
    getFromCache: function (cacheName, callback) {
        var storageName = cacheName + "_cache", defaultDictionary = {};
        defaultDictionary[storageName] = [];
        chrome.storage.sync.get(defaultDictionary, callback);
    },
    getInbox: function () {
        var that = this;
        /*this.consume({
            method: "GET",
            endpoint: "cases",
            params: {
                "workspace": this._pmWorkspace
            }
        }, {
            success: function (data) {
                var i;
                that._$body.empty();
                for (i = 0; i < data.length; i += 1) {
                    that.addItemToBody(new Case());
                }
            }
        });*/
        this.consume({
            method: "GET",
            endpoint: "cases",
            params: {
                "workspace": this._pmWorkspace
            }
        }, {
            success: function (data) {
                var i;
                that._pendantUpdates.inbox = false;
                jQuery(o.getItem('inbox')._html).removeClass("alert");
                that.addToCache("inbox", data, function (d) {
                    var data = this.args[1]["inbox_cache"];
                    console.log("inbox " + data.length + " elements.");
                    that._$body.empty();
                    for (i = 0; i < data.length; i += 1) {
                        that.addItemToBody(new Case({
                            id: data[i].app_uid,
                            name: data[i].app_title,
                            currentTask: data[i].app_tas_title,
                            dueDate: data[i].del_task_due_date,
                            process: data[i].app_pro_title,
                            onClose: that.onRemoveItem()
                        }));
                    }
                });
            }
        });
    },
    onRemoveItem: function () {
        var that = this;
        return function (item) {
            var cache_name;

            if (item instanceof Case) {
                cache_name = "inbox";
            } else if (item instanceof Track) {
                cache_name = "tracking";
            } else if (item instanceof Note) {
                cache_name = "notes";
            }

            that.getFromCache(cache_name, function (data) {
                var i, data = data[cache_name + "_cache"],
                    defaultDictionary = {};
                console.log("dfgdsgfghdfghdfghdfghdfg");
                for (i = 0; i < data.length; i += 1) {
                    if (cache_name !== 'notes') {
                        if (data[i].app_uid === item._id) {
                            data.splice(i, 1);
                            break;
                        }
                    } else {
                        if (data[i].uid === item._id) {
                            data.splice(i, 1);
                            break;
                        }
                    }

                }
                console.log(cache_name + "_cache", data.length);
                defaultDictionary[cache_name + "_cache"] = data;
                chrome.storage.sync.set(defaultDictionary);
            });

        }
    },
    getNotes: function () {
        var that = this;
        this.consume({
            method: "GET",
            endpoint: "HallooApi/Notes"
        }, {
            success: function (data) {
                var i;
                that._pendantUpdates.notes = false;
                jQuery(o.getItem('notes')._html).removeClass("alert");
                that.addToCache("notes", data, function () {
                    var data = this.args[1]["notes_cache"];
                    console.log("notes " + data.length + " elements.");
                    that._$body.empty();
                    for (i = 0; i < data.length; i += 1) {
                        that.addItemToBody(new Note({
                            id: data[i].uid,
                            author: data[i].firstname + " " + data[i].lastname,
                            message: data[i].note_content,
                            datetime: data[i].note_date,
                            avatar: 'img/avatar.jpg',
                            onClose: that.onRemoveItem()
                        }));
                    }
                });
            }
        });
    },
    getTracking: function () {
        var that = this;
        this.consume({
            method: "GET",
            endpoint: "cases/participated"
        }, {
            success: function (data) {
                var i;
                that._pendantUpdates.tracking = false;
                jQuery(o.getItem('tracking')._html).removeClass("alert");
                that.addToCache("tracking", data, function () {
                    var data = this.args[1]["tracking_cache"];
                    console.log("tracking " + data.length + " elements.");
                    that._$body.empty();
                    for (i = 0; i < data.length; i += 1) {
                        that.addItemToBody(new Track({
                            id: data[i].app_uid,
                            name: data[i].app_title,
                            process: data[i].app_pro_title,
                            eventDescription: "[event description]",
                            onClose: that.onRemoveItem()
                        }));
                    }
                });
            }
        });
    },
    receiveBroadcast: function (message) {
        var key, button;
        console.log("receiveBroadcast ", message);
        for (key in message) {
            if (message.hasOwnProperty(key)) {
                if (message[key]) {
                    if (key === this._currentSection) {
                        this[this._gettersMap[this._currentSection]].call(this);
                    } else {
                        this._pendantUpdates[key] = true;
                        button = o.getItem(key);
                        jQuery(button._html).addClass("alert");
                    }
                } else {
                    button = o.getItem(key);
                    jQuery(button._html).removeClass("alert");
                }
            }
        }
    },
    _gettersMap: {
        inbox: "getInbox",
        notes: "getNotes",
        tracking: "getTracking"
    },
    openURL: function (url) {
        chrome.tabs.create({'url': url});
    },
    showCase: function (id) {
        this.openURL('http://' + this._pmServer + '/sys' + this._pmWorkspace + '/en/neoclassic/cases/open?APP_UID=' + id + '&DEL_INDEX=2&action=todo');
    }
};

Halloo.init();

var o = new ButtonGroup({
    items: [
        {
            id: "inbox",
            text: "Inbox",
            onClick: function () {
                Halloo._currentSection = "inbox";
                if (Halloo._pendantUpdates["inbox"]) {
                    Halloo.getInbox();
                } else {
                    Halloo.getFromCache('inbox', function (data) {
                        var i;
                        data = data['inbox_cache'];
                        Halloo._$body.empty();
                        for (i = 0; i < data.length; i += 1) {
                            Halloo.addItemToBody(new Case({
                                id: data[i].app_uid,
                                name: data[i].app_title,
                                currentTask: data[i].app_tas_title,
                                dueDate: data[i].del_task_due_date,
                                process: data[i].app_pro_title,
                                onClose: Halloo.onRemoveItem()
                            }));
                        }
                    });
                }
            }
        }, {
            id: "notes",
            text: "Notes",
            onClick: function () {
                Halloo._currentSection = "notes";
                if (Halloo._pendantUpdates["notes"]) {
                    Halloo.getNotes();
                } else {
                    Halloo.getFromCache('notes', function (data) {
                        var i;
                        data = data['notes_cache'];
                        Halloo._$body.empty();
                        for (i = 0; i < data.length; i += 1) {
                            Halloo.addItemToBody(new Note({
                                id: data[i].uid,
                                author: data[i].firstname + " " + data[i].lastname,
                                message: data[i].note_content,
                                datetime: data[i].note_date,
                                avatar: 'img/avatar.jpg',
                                onClose: Halloo.onRemoveItem()
                            }));
                        }
                    });
                }
            }
        }, {
            id: "tracking",
            text: "Tracking",
            onClick: function () {
                Halloo._currentSection = "tracking";
                if (Halloo._pendantUpdates["tracking"]) {
                    Halloo.getTracking();
                } else {
                    Halloo.getFromCache('tracking', function (data) {
                        var i;
                        data = data['tracking_cache'];
                        Halloo._$body.empty();
                        for (i = 0; i < data.length; i += 1) {
                            Halloo.addItemToBody(new Track({
                                id: data[i].app_uid,
                                onClose: Halloo.onRemoveItem()
                            }));
                        }
                    });
                }
            }
        }
    ]
});
document.getElementById('button-group-container').appendChild(o.getHTML());
Halloo._currentSection = "inbox";

var port = chrome.extension.connect({name: "Sample Communication"});
//port.postMessage("Hi BackGround");
port.onMessage.addListener(function(msg) {
      Halloo.receiveBroadcast(msg);
});