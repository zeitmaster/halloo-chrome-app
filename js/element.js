var Element = function (settings) {
    this._html = null;
    this._id = null;
    Element.prototype.init.call(this, settings);
};

Element.prototype.init = function (settings) {
    var defaults = {
        id: Utils.generateUniqueId()
    };
    jQuery.extend(true, defaults, settings);
    this._id = defaults.id;
};

Element.prototype.createHTML = function () {};

Element.prototype.getHTML = function () {
    if (!this._html) {
        this.createHTML();
    }
    return this._html;
};