var ButtonGroup = function (settings) {
    Element.call(this, settings);
    this._items = [];
    ButtonGroup.prototype.init.call(this, settings);
};

ButtonGroup.prototype = new Element();
ButtonGroup.prototype.constructor = ButtonGroup;

ButtonGroup.prototype.init = function (settings) {
    var defaults = {
        items: []
    };

    jQuery.extend(true, defaults, settings);

    this.setItems(defaults.items);
};

ButtonGroup.prototype.getItem = function (itemID) {
    var i;
    for (i = 0; i < this._items.length; i += 1) {
        if (this._items[i]._id === itemID) {
            return this._items[i];
        }
    }
    return null;
};

ButtonGroup.prototype.addItem = function (item) {
    var newButton = new Button(item);

    this._items.push(newButton);

    return this;
};

ButtonGroup.prototype.setItems = function (items) {
    var i;

    for (i = 0; i < items.length; i += 1) {
        this.addItem(items[i]);
    }
    return this;
};

ButtonGroup.prototype.createHTML = function () {
    var i, clear;
    if (!this._html) {
        this._html = document.createElement('div');
        this._html.className = 'halloo-button-group';

        for (i = 0; i < this._items.length; i += 1) {
            this._html.appendChild(this._items[i].getHTML());
        }
        clear = document.createElement("div");
        clear.style.clear = 'both';
        this._html.appendChild(clear);
    }
    return this._html;
};