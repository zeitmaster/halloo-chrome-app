var fields, form, message;

function saveSettings(e) {
    var key, isValid = true, settingsToSave = {};
    e.preventDefault();
    for (key in fields) {
        if (fields.hasOwnProperty(key)) {
            if (!fields[key].value) {
                isValid = false;
                break;
            }
            settingsToSave[key] = fields[key].value;
        }
    }
    if (!isValid) {
        showMessage("All the fields are required.", true);
        return;
    } else {
        clearMessage();
    }
    Halloo.saveSettings(settingsToSave, function (e) {
        showMessage("Settings saved successfully!");
    });
}

function showMessage (msg, error) {
    message.textContent = msg;
    message.className = error ? "error" : "";
}

function clearMessage () {
    message.textContent = "";
}

function trimField(e) {
    var field = e.target;
    field.value = field.value.trim();
}

function init() {
    var key;
    form = document.getElementById('settings-form');
    form.addEventListener("submit", saveSettings);
    message = document.getElementById("message");
    fields = {
        "server": document.getElementById("server"),
        "workspace": document.getElementById("workspace"),
        "client_id": document.getElementById("client_id"),
        "app_id": document.getElementById("app_id"),
        "username": document.getElementById("username"),
        "password": document.getElementById("password")
    };
    for (key in fields) {
        if (fields.hasOwnProperty(key)) {
            fields[key].addEventListener('blur', trimField);
        }
    }
    Halloo.getSettings(function(settings) {
        var key;
        for (key in settings) {
            fields[key].value = settings[key];
        }
    });
}

document.addEventListener("DOMContentLoaded", init);