var Case = function (settings) {
    Element.call(this, settings);
    this._name = null;
    this._process = null;
    this._dueDate = null;
    this._currentTask = null;
    this._dom = {};
    this.onClose = null;
    Case.prototype.init.call(this, settings);
};

Case.prototype = new Element();
Case.prototype.constructor = Case;

Case.prototype.init = function (settings) {
    var defaults = {
        name: "[case _name]",
        process: "[process name]",
        dueDate: "[due date]",
        currentTask: '[current task]',
        onClose: null
    };

    jQuery.extend(true, defaults, settings);

    this._name = defaults.name;
    this._process = defaults.process;
    this._currentTask = defaults.currentTask;
    this._dueDate = defaults.dueDate;
    this.onClose = defaults.onClose;
};

Case.prototype.pause = function () {
    var that = this;
    Halloo.proxy.PUT({
        url: Halloo._apiURL + "/cases/" + this._id + "/pause",
        params: {
            "workspace": Halloo._pmWorkspace,
            "app_uid": this._id
        },
        headers: {
            "Authorization": "Bearer " + Halloo._token
        }
    }, {
        before: function () {
            that._dom.footer.style.display = 'none';
            that.showLoadingMessage("Pausing case...", 'fa fa-circle-o-notch fa-spin');
        },
        success: function () {
            that._dom.footer.style.display = '';
            that.hideLoadingMessage();
        },
        failure: function (errorMessage, errorCode) {
            that.showLoadingMessage('[' + errorCode + ']: ' + errorMessage);
            setTimeout(function () {
                that._dom.footer.style.display = '';
                that.hideLoadingMessage();
            }, 5000);
        },
        error: function () {
            //that.showLoadingMessage("ERROR");
            //setTimeout(function () {
                that._dom.footer.style.display = '';
                that.hideLoadingMessage();
                (Halloo.onRemoveItem())(that);
                jQuery(that._html).remove();
            //}, 5000);
        }
    });
};

Case.prototype.cancel = function () {
    var that = this;
    Halloo.proxy.PUT({
        url: Halloo._apiURL + "/cases/" + this._id + "/cancel",
        params: {
            "workspace": Halloo._pmWorkspace,
            "app_uid": this._id
        },
        headers: {
            "Authorization": "Bearer " + Halloo._token
        }
    }, {
        before: function () {
            that._dom.footer.style.display = 'none';
            that.showLoadingMessage("Canceling case...", 'fa fa-circle-o-notch fa-spin');
        },
        success: function () {
            that._dom.footer.style.display = '';
            that.hideLoadingMessage();
        },
        failure: function (errorMessage, errorCode) {
            that.showLoadingMessage('[' + errorCode + ']: ' + errorMessage);
            setTimeout(function () {
                that._dom.footer.style.display = '';
                that.hideLoadingMessage();
            }, 5000);
        },
        error: function () {
            //that.showLoadingMessage("ERROR");
            //setTimeout(function () {
                that._dom.footer.style.display = '';
                that.hideLoadingMessage();
                (Halloo.onRemoveItem())(that);
                jQuery(that._html).remove();
            //}, 5000);
        }
    });
};

Case.prototype.showLoadingMessage = function (message, icon) {
    if (this._html) {
        this._dom.messageAreaIcon.className = icon || "";
        this._dom.messageAreaLabel.textContent = message;
        jQuery(this._dom.messageArea).show();
    }
    return this;
};

Case.prototype.hideLoadingMessage = function () {
    jQuery(this._dom && this._dom.messageArea).hide();
    return this;
};

Case.prototype._attachListeners = function () {
    var that = this;
    jQuery(this._dom.closeButton).on("click", function (e) {
        e.preventDefault();
        jQuery(that._html).remove();
        if (typeof that.onClose === 'function') {
            that.onClose(that);
        }
    });
};

Case.prototype.view = function () {
    Halloo.showCase(this._id);
    return this;
};

Case.prototype.route = function () {
    Halloo.consume({
        method: 'PUT',
        endpoint: "cases/" + this._id + "/route-case",
        params: {
            "workspace": Halloo._pmWorkspace,
            "app_uid": this._id
        }
    }, {
        success: function () {
            console.log("scucess route");
        },
        failure: function () {
            console.log("fail route");
        }
    });
};

Case.prototype.createHTML = function () {
    var that = this, html, detailsList, close, footer, i, li, icon, label, messageArea, details = [{
        listClass: "halloo-case-name",
        iconClass: "fa fa-bolt",
        value: this._name
    }, {
        listClass: "halloo-case-process",
        iconClass: "fa fa-gears",
        value: this._process
    }, {
        listClass: "halloo-case-calendar",
        iconClass: "fa fa-calendar",
        value: this._dueDate
    }, {
        listClass: "halloo-case-task",
        iconClass: "fa fa-wrench",
        value: this._currentTask
    }], button, buttons = [{
        icon: "fa fa-eye",
        text: "View Case",
        callback: "view"
    }, {
        icon: "fa fa-play",
        text: "Router",
        callback: "route"
    }, {
        icon: "fa fa-stop",
        text: "Cancel",
        callback: "cancel"
    }, {
        icon: "fa fa-pause",
        text: "Pause",
        callback: "pause"
    }];

    if (!this._html) {
        html = document.createElement('li');
        html.className = 'halloo-case';

        detailsList = document.createElement("ul");
        detailsList.className = 'halloo-case-details';

        for (i = 0; i < details.length; i += 1) {
            li = document.createElement("li");
            li.className = details[i].listClass;

            icon = document.createElement("i");
            icon.className = details[i].iconClass;

            label = document.createElement("span");
            label.textContent = details[i].value;

            li.appendChild(icon);
            li.appendChild(label);

            detailsList.appendChild(li);
        }

        messageArea = document.createElement('div');
        messageArea.className = "halloo-case-message";
        icon = document.createElement('i');
        icon.className = 'halloo-case-message-icon';
        label = document.createElement('span');
        messageArea.appendChild(icon);
        messageArea.appendChild(label);

        close = document.createElement('span');
        close.className = "close fa fa-close";

        footer = document.createElement('footer');
        footer.className = "halloo-case-footer";

        this._dom.footer = footer;
        this._dom.messageArea = messageArea;
        this._dom.messageAreaIcon = icon;
        this._dom.messageAreaLabel = label;
        this._dom.closeButton = close;

        for (i = 0; i < buttons.length; i += 1) {
            button = new Button({
                text: buttons[i].text,
                icon: buttons[i].icon,
                onClick: (function (callback) {
                    return function () {
                        that[callback]();
                    }
                }(buttons[i].callback))
            });

            footer.appendChild(button.getHTML());
        }

        html.appendChild(detailsList);
        html.appendChild(messageArea);
        html.appendChild(close);
        html.appendChild(footer);

        this._html = html;

        this._attachListeners();
    }
    return this._html;
};