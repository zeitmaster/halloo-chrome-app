var Track = function (settings) {
    Element.call(this, settings);
    this._name = null;
    this._process = null;
    this._eventDescription = null;
    this._dom = {};
    this.onClose = null;
    Track.prototype.init.call(this, settings);
};

Track.prototype = new Element();
Track.prototype.constructor = Track;

Track.prototype.init = function (settings) {
    var defaults = {
        name: "[case _name]",
        process: "[process name]",
        eventDescription: "[event description]",
        onClose: null
    };

    jQuery.extend(true, defaults, settings);

    this._name = defaults.name;
    this._process = defaults.process;
    this._eventDescription = defaults.eventDescription;
    this.onClose = defaults.onClose;
};

Track.prototype._attachListeners = function () {
    var that = this;
    jQuery(this._dom.closeButton).on("click", function (e) {
        e.preventDefault();
        jQuery(that._html).remove();
        if (typeof that.onClose === 'function') {
            that.onClose(that);
        }
    });
};

Track.prototype.createHTML = function () {
    var html, detailsList, close, eventArea, footer, i, li, icon, label, messageArea, details = [{
        listClass: "halloo-case-name",
        iconClass: "fa fa-bolt",
        value: this._name
    }, {
        listClass: "halloo-case-process",
        iconClass: "fa fa-gears",
        value: this._process
    }, ], button, buttons = [{
        icon: "fa fa-eye",
        text: "View Case",
        callback: this.view
    }];

    if (!this._html) {
        html = document.createElement('li');
        html.className = 'halloo-case';

        detailsList = document.createElement("ul");
        detailsList.className = 'halloo-case-details';

        for (i = 0; i < details.length; i += 1) {
            li = document.createElement("li");
            li.className = details[i].listClass;

            icon = document.createElement("i");
            icon.className = details[i].iconClass;

            label = document.createElement("span");
            label.textContent = details[i].value;

            li.appendChild(icon);
            li.appendChild(label);

            detailsList.appendChild(li);
        }

        messageArea = document.createElement('div');
        messageArea.className = "halloo-case-message";
        icon = document.createElement('i');
        icon.className = 'halloo-case-message-icon';
        label = document.createElement('span');
        messageArea.appendChild(icon);
        messageArea.appendChild(label);

        close = document.createElement('span');
        close.className = "close fa fa-close";

        eventArea = document.createElement('div');
        eventArea.className = "halloo-case-event-message";
        eventArea.textContent = this._eventDescription;

        footer = document.createElement('footer');
        footer.className = "halloo-case-footer";

        this._dom.footer = footer;
        this._dom.messageArea = messageArea;
        this._dom.messageAreaIcon = icon;
        this._dom.messageAreaLabel = label;
        this._dom.closeButton = close;

        for (i = 0; i < buttons.length; i += 1) {
            button = new Button({
                text: buttons[i].text,
                icon: buttons[i].icon,
                onClick: buttons[i].callback
            });

            footer.appendChild(button.getHTML());
        }

        html.appendChild(detailsList);
        html.appendChild(eventArea);
        html.appendChild(messageArea);
        html.appendChild(close);
        html.appendChild(footer);

        this._html = html;
        this._attachListeners();
    }
    return this._html;
};