var Note = function (settings) {
    Element.call(this, settings);
    this._author = null;
    this._message = null;
    this._datetime = null;
    this._avatar = null;
    this._dom = {};
    this.onClose = null;
    Note.prototype.init.call(this, settings);
};

Note.prototype = new Element();
Note.prototype.constructor = Note;

Note.prototype.init = function (settings) {
    var defaults = {
        author: '[author]',
        message: '[message]',
        datetime: '[datetime]',
        avatar: 'img/avatar.jpg',
        onClose: null
    };
    jQuery.extend(true, defaults, settings);

    this._author = defaults.author;
    this._message = defaults.message;
    this._datetime = defaults.datetime;
    this._avatar = defaults.avatar;
    this.onClose = defaults.onClose;
};

Note.prototype._attachListerners = function () {
    var that = this;
    jQuery(this._dom.closeButton).on("click", function (e) {
        e.preventDefault();
        jQuery(that._html).remove();
        if (typeof that.onClose === 'function') {
            that.onClose(that);
        }
    });
};

Note.prototype.createHTML = function () {
    var li,
        img,
        content,
        title,
        username,
        datetime,
        message,
        clear,
        textContainer,
        close;

    if (!this._html) {
        li = document.createElement('li');
        li.className = "halloo-note";

        img = document.createElement('img');
        img.className = "halloo-avatar";
        img.src = this._avatar;

        content = document.createElement('div');
        content.className = "halloo-content";

        title = document.createElement('div');
        title.className = "halloo-title";

        username = document.createElement('span');
        username.className = "halloo-username";
        username.textContent = this._author;

        datetime = document.createElement('span');
        datetime.className = "halloo-datetime";
        datetime.textContent = this._datetime;

        message = document.createElement('div');
        message.className = "halloo-message";
        message.appendChild(datetime);
        textContainer = document.createElement('div');
        textContainer.className = "halloo-message-text";
        textContainer.textContent = this._message;
        message.appendChild(textContainer);

        close = document.createElement('span');
        close.className = "close fa fa-close";

        clear = document.createElement('div');
        clear.className = "clear";

        title.appendChild(username);
        content.appendChild(title);
        content.appendChild(message);
        content.appendChild(close);

        this._dom.closeButton = close;

        li.appendChild(img);
        li.appendChild(content);
        li.appendChild(clear);

        this._html = li;

        this._attachListerners();
    }
    return this._html;
};