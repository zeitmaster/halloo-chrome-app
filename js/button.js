var Button = function (settings) {
    Element.call(this, settings);
    this._text = null;
    this._icon = null;
    this.onClick = null;

    Button.prototype.init.call(this, settings);
};

Button.prototype = new Element();
Button.prototype.constructor = Button;

Button.prototype.init = function (settings) {
    var defaults = {
        text: '[button]',
        icon: '',
        onClick: null
    };

    jQuery.extend(true, defaults, settings);
    this._text = defaults.text;
    this._icon = defaults.icon;
    this.onClick = defaults.onClick;
};

Button.prototype._attachListeners = function () {
    var that = this;
    if (this._html) {
        this._html.addEventListener('click', function (e) {
            e.preventDefault();
            if (typeof that.onClick === 'function') {
                that.onClick(that);
            }
        });
    }
    return this;
};

Button.prototype.createHTML = function () {
    var html, icon;
    if (!this._html) {
        html = document.createElement('a');
        html.className = 'halloo-button';
        html.href = "#";
        icon = document.createElement('i');
        icon.className = this._icon;
        html.appendChild(icon);
        html.appendChild(document.createTextNode(this._text));
        this._html = html;
        this._attachListeners();
    }
    return this._html;
};